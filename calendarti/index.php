<?php
include_once 'includes/db_connect.php';
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<?php
date_default_timezone_set("Asia/Bangkok");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Calendar Sign in </title>
	<link rel="icon" type="image/png" href="pic/icon.png">

	<link rel="stylesheet" type="text/css" href="style/indexstyle.css" />
	<link rel="stylesheet" type="text/css" href="style/signinStyle.css" />
	<link rel="stylesheet" href="styles/main.css" />

    	<script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>


    	<meta charset="UTF-8">
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
		<link rel="stylesheet" href="styles/main.css" /> 
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script> 
$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle("slow");
  });
});
</script>
<style>
.button_title{
	background-color: #56a4ef;
	border: none;
	color: white;
	padding: 5px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	margin: 5px;
	font-size: 16px;	
	cursor: pointer;
	border-radius: 8px;
}
</style>

</head>

<body>

<!--sign in-->
<div class="manuN">
	<div class="errorlogin">
		<?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
    	?>
	</div>
		<div class="tabText">
<form action="includes/protected_page.php" 
	    method="post" 
	    name="login_form"> 
	E-mail: <input type="text" 
				  name="email" 
				  placeholder="Email" 
				  class="inputSingin" />
	Password: <input type="password" 
                   name="password" 
                   id="password" 
                   placeholder="Password" class="inputSingin">
	 <input type="button" 
          value="Sign In" 
          class="singinButton"
          onclick="formhash(this.form, this.form.password);" /> <br>
	
		</div>
</form>
</div>
 
 <!--sing up form-->
<div id="flip" class="crecateBox"><b><button class="button_title">Create an Account</button></b></div>
<div id="panel"><h2>Sign Up Form</h2>
	<?php
	if (!empty($error_msg)) {
            echo $error_msg;
        }
    ?>
    
<form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
	<label><b>Username </b></label><br>
    <input type='text' 
    	   name='username' 
    	   id='username' 
    	   required class="inputSingup" />
    	   <br><br>   	   
    <label><b>Email</b></label><br>
    <input type="text" 
    	   name="email" 
    	   id="email" 
    	   required class="inputSingup"/>
    	   <br><br>
    <label><b>Password</b></label><br>
    <input type="password"
           name="password" 
           id="password" 
           required class="inputSingup">
           <br><br>
    <label><b>Confirm password</b></label><br>
    <input type="password" 
           name="confirmpwd" 
		   id="confirmpwd"  
           required class="inputSingup">
           <br><br>
    <input type="button" 
		   value="Register"
		   class="button" 
           onclick="return regformhash(this.form,
                                       this.form.username,
                                       this.form.email,
                                   	   this.form.password,
                                       this.form.confirmpwd);" /> 	
                                       <br><br>
            Usernames may contain only digits, upper and lower case letters and underscores<br>
            Emails must have a valid email format<br>
            Passwords must be at least 6 characters long<br>
            Passwords must contain <br>
                    At least one upper case letter (A..Z)<br>
                    At least one lower case letter (a..z)<br>
                    At least one number (0..9)<br>
            
            Your password and confirmation must match exactly<br><br>

</div>
</form>
</div></div>

<!-- show today-->
<div class="nowAday">
<script language="javascript">//show date 
	now = new Date(); 
	var thday = new Array ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	var thmonth = new Array ("January","February","March","April","May" ,"June" ,"July" ,"August", "September", "October" ,"November","December");

document.write(thday[now.getDay()]+ " " + now.getDate()+ " " + thmonth[now.getMonth()] + " " + (0+now.getYear()+1900));
</script>
</div>

<!-- calendar -->

<?php include_once 'engine/indexcalendar.php'; ?>
</div>

</html>