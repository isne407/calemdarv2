/*database name secure_login*/
CREATE TABLE `secure_login`.`members` ( 
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`username` VARCHAR(30) NOT NULL, 
	`email` VARCHAR(50) NOT NULL, 
	`password` CHAR(128) NOT NULL, 
	`salt` CHAR(128) NOT NULL 
) ENGINE = InnoDB;


CREATE TABLE `secure_login`.`login_attempts` ( 
	`user_id` int(11) NOT NULL, 
	`time` VARCHAR(30) NOT NULL 
) ENGINE=InnoDB

/* database calendar for create event in calendar*/

CREATE TABLE `appointment` ( 
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`title` VARCHAR(30) NOT NULL, 
	`dates` date NOT NULL, 
	`details` text(128) NOT NULL, 
	`username` CHAR(52) NOT NULL,
	`times` TIME 
) ENGINE = InnoDB;

