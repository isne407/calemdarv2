<!DOCTYPE html>
<html>
<head>
    <title>Calendar </title>
    <link rel="icon" type="image/png" href="pic/icon.png">
    <link rel="stylesheet" type="text/css" href="style/calendarStyle.css">
    <link rel="stylesheet" type="text/css" href="style/signinStyle.css">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles/main.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<script src="jquery-1.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script> 
$(document).ready(function(){
  $(".topic").click(function(){
    $(this).children().toggle();
  });
});
</script>
<style>
.detail{
    position: relative;
    border :solid 1px;
    width: 250px;
    height: 250px;
    display: none;
    background: #F2F2F2;
    
}
</style>

<!--pop up -->
<script>
            $(document).ready(function (){
                $(".open").click(function (){
                    $(".pop-outer").fadeIn("slow");
                });
                $(".close").click(function (){
                    $(".pop-outer").fadeOut("slow");
                });
            });
        </script>

<style>
    .pop-outer {
        background-color: rgba(0, 0, 0, 0.5);
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
            }
    .pop-inner {
        background-color: #fff;
        width: 500px;
        height: 300px;
        padding: 25px;
        margin: 15% auto;
        font-family: 'Raleway', sans-serif;
        font-size: 15px;
        font-weight: bold;
            }
</style>
</head>

<body>
    
<div class="manuN">
        <div class="tabText">
<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
?>
            <?php if (login_check($mysqli) == true) : ?>
        Welcome <?php echo htmlentities($_SESSION['user']); 
            $username = $_SESSION['user'];
        ?>
        <a href="includes/logout.php"><button class="singinButton">Sign out</button></a>
        <?php else : ?>        
                <span class="error">You are not authorized to access this page.</span> Please <a href="index.php">login</a>.
        <?php endif; ?>
        </div>
</form>
</div>
<br>

<!-- post-->

<div class="calendarminiblock">
    <div class="titleBox">Edit Here!</div><br>

<form action="update.php" method="post">
    <?php $idEvent = $_POST['id'];?>
    <br>
        Title :   <input type="text" name="title" class="title"></br></br>
        Date :    <input type="date" name="dates"><br /><br>
        Time : <input type="time" name="time"><br><br>
        Detail : <textarea name="details" cols="40" rows="4" class="description"></textarea></br></br>
        <input type="text" name="idEvent" value="<?php  echo $idEvent?>" hidden>
    
  <center> <input type="submit" value="Update" class="button"> </center>
</form>
</div>
<!-- end post -->

<!-- switch monthview ,dayview and weekview-->
<div class="container">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Month view</a></li>
    <li><a data-toggle="tab" href="#menu1">Week view</a></li>
    <li><a data-toggle="tab" href="#menu2">Day view</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        
<!-- show today-->
<div class="nowAday">
<script language="javascript">
    now = new Date(); 
    var thday = new Array ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"); 
    var thmonth = new Array ("January","February","March","April","May" ,"June" ,"July" ,"August", "September", "October" ,"November","December");
document.write(thday[now.getDay()]+ " " + now.getDate()+ " " + thmonth[now.getMonth()] + " " + (now.getYear()+1900));

</script>
</div>


<div class="blockCalendar">
<?php
	$date = strtotime(date("Y-m-d"));
	$day = date('d', $date);
	$realmonth = date('m', $date);
	$month = date('m', $date);
	$realyear = date('Y', $date);
	$year = date('Y', $date);

	$daysInMonth = cal_days_in_month(0, $month, $year);
	$blank = date('w', strtotime("{$year}-{$month}-01"));

	if(isset($_GET["year"])){
		$year = $_GET["year"];
	}
	if(isset($_GET["month"])){
		$month = $_GET["month"];
		if($month > 12) {
			$year = $year+1;
			$month = 1;
		}
		else if($month < 1){
			$year = $year-1;
			$month = 12;
		}
			$daysInMonth = cal_days_in_month(0, $month, $year);
			$blank = date('w', strtotime("{$year}-{$month}-01"));
	}
	
	if($year != $realyear){
		$daysInMonth = cal_days_in_month(0, $month, $year);
		$blank = date('w', strtotime("{$year}-{$month}-01"));
	}
	if($month == 1 && $year == 2017){$blank = 7;}
?>
<br>
	<div class="calendar">
	<!-- Month / nextMonth, prevMonth -->
	<div class="month">
    <span class="next"><a href="calendar.php?month=<?php echo $month+1; ?>&year=<?php echo $year ?>"><img src="pic/next.png" class="n"></a></span>
	<span class="prev"><a href="calendar.php?month=<?php echo $month-1; ?>&year=<?php echo $year ?>"><img src="pic/prev.png" class="n"></a></span>
	<?php $dateObj = DateTime::createFromFormat('!m', $month);
		echo $dateObj->format('F');
    ?>
	</div><!-- div month-->
    <div class="week">
        <span class="dayname">Sunday</span>
		<span class="dayname">Monday</span>
		<span class="dayname">Tuesday</span>
		<span class="dayname">Wednesday</span>
		<span class="dayname">Thursday</span>
		<span class="dayname">Friday </span>
		<span class="dayname">Saturday</span>	
	</div><!-- div week-->
		<?php for($i = 1;$i <= $blank;$i++){ 
			echo '<div class="blank"></div>';
        }
        ?>
        
<?php

$firstday = date('w',strtotime('01-'.$month.'-'.$year));
$today = date('d');
$todaymonth = date('m');
$todayyear = date('Y');
?>

<?php

//number of month
for ($i=1;$i<=$daysInMonth;$i++){ /// test number, day month 
    echo'<div class="dateC'; // = div class="dateC">$i
    echo '">'.$i.'<br>';

    if ($today==$i && $month==$todaymonth && $year==$todayyear) { // today 
        echo '<div class="today"></div>';
    }
    // show  events in calendar table 
    $username = $_SESSION["user"];
    $mysqli = new mysqli("localhost", "root","", "calendar");
    $qry = "SELECT * FROM appointment";
    $result = $mysqli->query($qry);

    while ($row = $result->fetch_array()){
           $dbday = date('d',strtotime($row['dates']));
           $dbmonth = date('m',strtotime($row['dates']));
           $dbyear = date('Y',strtotime($row['dates']));
           $dbtitle = $row['title'];
           $dbdetail = $row['details'];
           $dbusername = $row['username'];
           $id = $row['id'];
           $dbdates = $row['dates'];
           $dbtimeShow = $row['times'];
           
           if($dbday==$i&&$dbmonth==$month&&$dbyear==$year&&$username==$dbusername){ 
            echo "<div class='dbtitle'>";
            
            echo '<div class="topic">';
            echo '<button class="button_title" class="open">'.$dbtitle.'</button>'.'<br>'.'<br>';
                echo '<div class="pop-outer" style="display : none;">';
                    echo '<div class="pop-inner">';
                        echo "Title : ".$dbtitle.'<br>'.'<br>';
                        echo "Date : ".$dbdates.'<br>'.'<br>';
                        echo "Time : ".$dbtimeShow.'<br>'.'<br>';
                        echo "Details : ".$dbdetail.'<br>'.'<br>';
                        echo "<a href='edit.php'><h4><b>EDIT HERE!</b></h4></a>";
                        echo '<div><center><form action="delete.php" method="post">
                                <input name = "id" type="text" value='.$id.' hidden><br>
                                <button  type="submit" class="delete">Delete</button>
                                </form></center></div>';
                                
                        echo '</div>';
                echo '</div>';
            echo '</div>';
            echo "</div>";
         } 
    }
            echo '</div>';
    } 
?>
</div><!-- div calendar-->
</div>
    </div>
    <div id="menu1" class="tab-pane fade">
    <?php require_once 'weekview.php'; ?>
    </div>
    <div id="menu2" class="tab-pane fade">
    <?php require_once 'dayview.php' ?>
    </div>
</body>
</html>
